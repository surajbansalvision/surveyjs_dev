Installation

In your terminal:
$ composer require composer require Surajbansal/laravel-surveyjs

Publish the config file & run the migrations

php artisan vendor:publish --provider="Surajbansal\LaravelSurveyJS\LaravelSurveyJsServiceProvider"
php artisan migrate

Create a new survey on your-project-domain/admin/survey

[optional] Change values in config/survey-manager.php (route prefix, middleware, builder theme etc.)
